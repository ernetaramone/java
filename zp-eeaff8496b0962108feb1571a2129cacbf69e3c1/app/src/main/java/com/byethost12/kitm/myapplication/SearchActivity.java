package com.byethost12.kitm.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.Button;

public class SearchActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        Button newEntrybtn = (Button) findViewById(R.id.entry_btn);
        newEntrybtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
                Intent goToEntryActivity = new Intent(SearchActivity.this, EntryActivity.class);//Pirmas param = is kurios veiklos, Antras į kuria veiklą
                startActivity(goToEntryActivity);
            }
        });
    }
}
