package com.byethost12.kitm.myapplication;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.HashMap;


public class EntryActivity extends AppCompatActivity {

    public static final String INSERT_URL="http://bambulis.byethost7.com/mobile/insert.php";
    String colorChecked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entry);

        String carBrands[]={
                getResources().getString(R.string.entry_brand_audi),
                getResources().getString(R.string.entry_brand_bmw),
                getResources().getString(R.string.entry_brand_seat)
        };


        final Spinner carBrand=(Spinner) findViewById(R.id.entry_brand);
        ArrayAdapter<String>brandAdapter=new ArrayAdapter
                (this,
                android.R.layout.simple_dropdown_item_1line,
                carBrands
                );
        brandAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        carBrand.setAdapter(brandAdapter);

        String carTypes[]={
                getResources().getString(R.string.entry_car_type_4x4),
                getResources().getString(R.string.entry_car_type_cabrio),
                getResources().getString(R.string.entry_car_type_sedan)
        };

        final Spinner carType=(Spinner) findViewById(R.id.entry_car_type);
        ArrayAdapter<String>typeAdapter=new ArrayAdapter
                (this,
                        android.R.layout.simple_dropdown_item_1line,
                        carTypes
                );
        typeAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        carType.setAdapter(typeAdapter);


        final RadioGroup entryColors=(RadioGroup) findViewById(R.id.entry_colors);
        final RadioButton entryColorBlack = (RadioButton) findViewById(R.id.entry_color_black);
        final RadioButton entryColorGreen = (RadioButton) findViewById(R.id.entry_color_green);
        final RadioButton entryColorSilver = (RadioButton) findViewById(R.id.entry_color_silver);

        final CheckBox carDeffectDamaged=(CheckBox)findViewById(R.id.entry_car_defects_damaged);
        final CheckBox carDeffectScratched=(CheckBox)findViewById(R.id.entry_car_defects_scratched);
        final CheckBox carDeffectDrowned=(CheckBox)findViewById(R.id.entry_car_defects_drowned);

        final EditText carYear=(EditText) findViewById(R.id.entry_year);

        final Button entrySubmitBtn=(Button) findViewById(R.id.entry_submit);

        entrySubmitBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (entryColorBlack.isChecked())
                {
                    colorChecked=entryColorBlack.getText().toString();

                }
                else if (entryColorGreen.isChecked())
                {
                    colorChecked=entryColorGreen.getText().toString();
                }
                else if (entryColorSilver.isChecked())
                {
                    colorChecked=entryColorSilver.getText().toString();
                }
                String carDefectChecked="";
                if (carDeffectDamaged.isChecked())
                {
                    carDefectChecked=carDefectChecked+carDeffectDamaged.getText().toString()+" ";
                }
                if (carDeffectScratched.isChecked())
                {
                    carDefectChecked=carDefectChecked+carDeffectScratched.getText().toString()+" ";
                }
                if (carDeffectDrowned.isChecked())
                {
                    carDefectChecked=carDefectChecked+carDeffectDrowned.getText().toString()+" ";
                }
                if (!carDeffectDamaged.isChecked()&&!carDeffectScratched.isChecked()&&!carDeffectDrowned.isChecked())
                {
                    carDefectChecked=getResources().getString(R.string.entry_car_defects_ok);
                }
                String selectBrandSpinner=carBrand.getSelectedItem().toString();
                String selectCarTypeSpinner=carType.getSelectedItem().toString();

                String enteredYear=carYear.getText().toString();

                if (!Validation.isValidYear(enteredYear))
                {
                    carYear.setError(getResources().getString(R.string.entry_error_year));
                    carYear.requestFocus();
                }
                else {
                    Autonobilis car= new Autonobilis(selectBrandSpinner,
                            colorChecked,
                           Integer.parseInt(enteredYear) ,
                            selectCarTypeSpinner,
                            carDefectChecked );

                            addToDb(car);

//                    Toast.makeText(EntryActivity.this,
//                            "Marke: " + car.getBrand() + "\n" +
//                                    "Spalva: " + car.getColor() + "\n" +
//                                    "Metai: " + car.getYear() + "\n" +
//                                    "Tipas: " + car.getType() + "\n" +
//                                    "Defektai: " + car.getDeffect(),
//                            Toast.LENGTH_LONG).show();
                }
            }


        });





    }
    private void addToDb(final Autonobilis coolCar)
    {
        class NewEntry extends AsyncTask<String, Void, String>
        {
            ProgressDialog loading;
            DB db = new DB();


            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading=ProgressDialog.show(EntryActivity.this,
                        getResources().getString(R.string.entry_db_msg), null, true, true);
            }

            @Override
            protected String doInBackground(String... strings) {
//pirsmas strinf raktas, antras reiksme
                HashMap<String, String> carData=new HashMap<String, String>();
                carData.put("marke", strings[0]);
                carData.put("spalva", strings[1]);
                carData.put("defektai", strings[2]);
                carData.put("kebulas", strings[3]);
                carData.put("metai", strings[4]);

                return null;
            }

            @Override
            protected void onPostExecute(String  s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(EntryActivity.this,s,Toast.LENGTH_LONG).show();
                Intent goToSearch=new Intent(EntryActivity.this,SearchActivity.class);
                startActivity(goToSearch);
            }
        }
        NewEntry newEntry=new NewEntry();
        newEntry.execute(coolCar.getBrand(),coolCar.getColor(),coolCar.getDeffect(),coolCar.getType(),Integer.toString(coolCar.getYear()));
    }

}
