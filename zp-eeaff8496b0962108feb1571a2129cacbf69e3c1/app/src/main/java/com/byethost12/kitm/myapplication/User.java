package com.byethost12.kitm.myapplication;


import android.content.Context;
import android.content.SharedPreferences;

public class User {

    private String userName;
    private String password;
    private String email;

    private SharedPreferences sharedPreferences;
    private static final String PREFERENCES_FILE_NAME="com.byethost12.kitm.myapplication";
    private static final String USERNAME_KEY="userName";
    private static final String PASSWORD_KEY="password";
    private static final String REMEMBERME_KEY="rememberMe";

    // skirtas prisijungimui
    public User(Context context)
    {
        this.sharedPreferences=context.getSharedPreferences(User.PREFERENCES_FILE_NAME,
                Context.MODE_PRIVATE);
    }

//reikes kai registruosim vartotoja
    public User(String userName, String password, String email) {
        this.userName = userName;
        this.password = password;
        this.email = email;
    }

    public User(String userName) {
        this.userName = userName;
    }


    public String getUserNameForRegister() {
        return userName;
    }
    public void setUserNameForRegister(String userName) {
        this.userName = userName;
    }

    public String getPasswordForRegister() {
        return password;
    }

    public void setPasswordForRegister(String password) {
        this.password = password;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

//PRISIJUNGIMO LANGUI

    public String  getUserNameForLogin( ) {
        return  this.sharedPreferences.getString(USERNAME_KEY,"");
    }
    public void setUserNameForLogin(String userName) {
        this.sharedPreferences.edit().putString(USERNAME_KEY, userName).commit();
    }

    public String getPasswordLogin() {
        return this.sharedPreferences.getString(PASSWORD_KEY,"");
    }

    public void setPasswordLogin(String password) {
        this.sharedPreferences.edit().putString(PASSWORD_KEY, password).commit();
    }

    public boolean isRemembered()
    {
        return this.sharedPreferences.getBoolean(REMEMBERME_KEY, false);
    }

    public void setRemembermeKey(boolean remembermeKey)
    {
     this.sharedPreferences.edit().putBoolean(REMEMBERME_KEY, remembermeKey).commit();
    }

}
