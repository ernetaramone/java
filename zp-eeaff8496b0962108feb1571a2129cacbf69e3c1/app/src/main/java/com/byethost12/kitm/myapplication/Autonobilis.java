package com.byethost12.kitm.myapplication;

public class Autonobilis {

    private String brand;
    private String color;
    private int year;
    private String type;
    private String deffect;


    public Autonobilis(String brand, String color, int year, String type, String deffect) {
        this.brand = brand;
        this.color = color;
        this.year = year;
        this.type = type;
        this.deffect = deffect;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDeffect() {
        return deffect;
    }

    public void setDeffect(String deffect) {
        this.deffect = deffect;
    }
}
